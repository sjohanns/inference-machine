
import os
import sys

sys.path.insert(0,
                os.path.abspath(
                    os.path.join(os.path.dirname(__file__),
                                 './src/')
                ))



from frontend.application import app, Config

if __name__ == '__main__':
    app.config.from_object(Config)
    app.run(port=5050)
