
__version__ = "0.0.1"

from setuptools import setup

setup(
    name="inference_machine",
    version=__version__,
    author="Johann Tovstolyak",
    author_email="tovstolaki62@gmail.com",
    description=(
        "The inference machine module to medsystem https://gitlab.com/sjohanns/medsystem "
    ),
    long_description=long_description,
    long_description_content_type="text/org",
    license="GPLv3"
)
