
import json
from bson.objectid import ObjectId

import backend.objects as objs
from backend.database import DB

from flask import Flask, request, jsonify

app = Flask(__name__)

patients = DB('patient_test')

diseases, symptoms = DB('diseases_test').data, DB('symptoms_test').data

inference = objs.InferenceMachine(diseases=diseases, symptoms=symptoms)

class ObjectIdDecoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, ObjectId):
            return str(obj)
        return json.JSONEncoder.default(self, obj)


class Config(object):
    DEBUG = True
#    SESSION_COOKIE_SECURE=True,
    SESSION_COOKIE_HTTPONLY=True,
#    SESSION_COOKIE_SAMESITE='Lax',
    SECRET_KEY = '833db5b8f120f755cd1484209322ab56ca80c536'

@app.route('/api.test', methods=['GET', 'POST'])
def test():
    if request.method == "GET":
        return jsonify({'status': True})

@app.route('/api.diagnostics', methods=['POST'])
def diagnos():
    if request.method == 'POST':

        patient_name = json.loads(request.data)
        if not patient_name['name']:
            return jsonify({'status': False, 'desc': 'Не правильний запит'})
        else:
            patient = patients.data.find_one({'name': patient_name['name']}, {})
            if not patient:
                return jsonify({'status': False, 'desc': 'Цього пацієнту немає в базі даних'})
            else:
                result = inference.inferenceMachine(patient, level=int(patient_name['level']))
                return jsonify(json.loads(json.dumps({'result': result, 'status': True}, cls=ObjectIdDecoder)))
