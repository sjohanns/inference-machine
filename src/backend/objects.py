
import math
from bson.int64 import Int64

class InferenceMachine():
    _costfuns = {'=': (lambda a, b: (lambda a, b: True)(a, b) if a == b else False),
                '>': (lambda a, b: (lambda a, b: (a - b)/a)(a, b) if b > a else False),
                '<': (lambda a, b: (lambda a, b: (b - a)/b)(a, b) if b < a else False),
                '>=': (lambda a, b: (lambda a, b: (a - b)/a)(a, b) if b >= a else False),
                '<=': (lambda a, b: (lambda a, b: (b - a)/b)(a, b) if b <= a else False)}
    def __init__(self, id=None, diseases=[], symptoms=[], level=None):
        self.id = id
        self.diseases = diseases
        self.symptoms = symptoms
        self.level = level
    def inferenceMachine(self, patient, level=1):
        level = self.level or level
        self.patient = patient
        self.costers = {}
        for i in range(level):
            listDiseases = self.search_diseases(self.patient)
            print(listDiseases)
            if listDiseases != -1:
                self.breadthCosted(listDiseases)
            else:
                return list(self.costers.values())
            patient['symptoms'] = [{'_id': obj['_id'], 'name': obj['name'], 'value': obj['cost'] * 100} for obj in self.costers.values() if obj['cost'] > 0]

        return list(self.costers.values())
    # TODO: symptoms or symptoms_id
    def search_diseases(self, patient):
        res_diseases = []
        symptoms_id_patient = set([symptom['_id'] for symptom in patient['symptoms']])

        for symptoms_disease in self.diseases.find({}, {'name': 1, '_id': 1, 'symptoms': 1}):
            symptoms_id_disease = set([symptom for symptom in symptoms_disease['symptoms']])
            if symptoms_id_patient & symptoms_id_disease:
                res_diseases.append(symptoms_disease['_id'])
        return res_diseases or -1
    def breadthCosted(self, neededDiseases):
         costed = 0
         stack = neededDiseases
         while stack:
             cur = stack.pop()
             costed = 0
             disease = self.diseases.find_one({'_id': cur}, {'name': 1, 'symptoms': 1})
             for symptom in disease['symptoms']:
                 find = [sym['value'] for sym in self.patient['symptoms'] if sym['_id'] == symptom]
                 if find:
                     find = find[0]
                     if self.costers.get(symptom, False):
                         costed += self.costers.get(symptom[0])['cost']
                     else:
                         value_symptom = (self.symptoms.find_one({'_id': symptom}, {'value': 1, 'cost': 1}))
                         costed += self.cost(value_symptom['value'], find) * value_symptom['cost']
             self.costers[disease['name']] = {'name': disease['name'],
                                              'cost': float("%.2f" % (costed / len(disease['symptoms']))),
                                              '_id': cur}
    def cost(self, value1, value2):
         #_formater = lambda n: abs(n) / 10**math.floor(math.log10(abs(n)))
         if isinstance(value1, bool):
             return (not value1 and not bool(value2)) or (value1 and bool(value2))
         if isinstance(value2, bool):
             return True and value2
         else:
             if isinstance(value1, list):
                 #formtv1 = _formater(value1[1])
                 #formtv2 = _formater(value2)
                 print(value1[1], value2[1])
                 return abs(self._costfuns[value1[0]](value1[1], value2[1]))
             else:
                 raise Exception('Not list value is')
