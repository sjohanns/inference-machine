
import pymongo

import os
import urllib
from dotenv_vault import load_dotenv
load_dotenv()

NAME = urllib.parse.quote_plus(os.getenv('NAME'))
PASSWORD = urllib.parse.quote_plus(os.getenv('PASSWORD'))

print(NAME, PASSWORD)

client = pymongo.MongoClient('localhost',
                      username=NAME,
                      password=PASSWORD,
                      authSource='medsystem')

class DB:
    db = client['medsystem']
    def __init__(self, name):
        self.data = self.db[name]
