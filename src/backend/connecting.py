
from warnings import warn

from urllib.parse import quote_plus

from pymongo import MongoClient
from pymongo.errors import ConnectionFailure

class OutCollection(Exception): pass
class WarningEmptyCollection(UserWarning): pass


def ispinged(cursor):
    try:
        cursor.admin.command('ping')
    except ConnectionFailure as err:
        import time
        import socket
        time.sleep(1000)
        print('You have a connection failure.\nMaybe there isn`t actived mongodb on this host %s\n%s' % (socket.gethostbyname(socket.gethostname()),err))

def connection(host_name='', user='', password=''):
    if host_name and user and password:
        url = 'mongodb://%s:%s@%s' % \
                (quote_plus(user), quote_plus(password), quote_plus(host_name))
        client = MongoClient(url)
    else:
        client = MongoClient()
    return client.medsystem

def get_diseases_symptoms(client):
    try:
        collections = [c['name'] for c in client.list_collections()]
        if not 'diseases' in collections:
            raise OutCollection('diseases')
        if not 'symptoms' in collections:
            raise OutCollection('symptoms')
        else:
            symptoms, diseases = client['symptoms'], client['diseases']
    except OutCollection as err:
        print('In this mongo session %s dont exists' % err)
        return None
    finally:
        acceptedEmpty(symptoms)
        acceptedEmpty(diseases)
        return symptoms, diseases

def acceptedEmpty(collection):
    res = collection.find({}, {})
    res = [obj for obj in res]
    if not res:
        warn('This collection %s is empty' % (collection.name), category=WarningEmptyCollection)
    return None



if __name__ == '__main__':
    CLIENT = MongoClient()
    try:
        CLIENT.admin.command('ping')
    except ConnectionFailure as err:
        import time
        import socket
        time.sleep(1000)
        print('You have a connection failure.\nMaybe there isn`t actived mongodb on this host %s\n%s' % (socket.gethostbyname(socket.gethostname()),err))
