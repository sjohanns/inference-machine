import pymongo
from sshtunnel import SSHTunnelForwarder

import string

import sys

import timeit

import os
from dotenv_vault import load_dotenv
load_dotenv()

import lib.objects as objs
from tests.patients.patient_gen import Patient
from tests.generator.diseases_gen import GenDiseases


IP = os.getenv('IP')
PORT = int(os.getenv('PORT'))
NAME = os.getenv('NAME')
PASSWORD = os.getenv('PASSWORD')

server = SSHTunnelForwarder(
    (IP, PORT),
    ssh_username=NAME,
    ssh_password=PASSWORD,
    remote_bind_address=('localhost', 27017))
server.start()
client = pymongo.MongoClient('localhost', server.local_bind_port)
db = client.medsystem_test


def main_test(d, s, k):
    gend = GenDiseases(d, d, s, s, db, string.ascii_letters, 20)
    gend.update()
    patient = Patient('Johann', db.symptoms, k)
    print(patient.symptoms)
    inference = objs.InferenceMachine(diseases=db.diseases, symptoms=db.symptoms)
    return (lambda: inference.inferenceMachine({'symptoms': patient.symptoms}))
