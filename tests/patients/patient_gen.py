import random

from lib.connecting import WarningEmptyCollection
from bson.objectid import ObjectId

class Patient:
    rand = random.Random(200)
    def __init__(self, name, base_symptoms, k):
        self.name = name
        self.dataSymptoms = base_symptoms
        self.k = k
        self._gen()
    def _gen(self):
        _symptoms = [obj['name'] for obj in self.dataSymptoms.find({}, {'_id': 1, 'name': 1})]
        try:
            if not _symptoms:
                raise WarningEmptyCollection('Symptoms is empty')
        except WarningEmptyCollection as err:
                print('WARNING: %s' % err)
        _symptoms = self.rand.sample(_symptoms, k=self.k)
        self.symptoms = []
        self.gen_value = {
            "=": lambda a: ['=', self.rand.choice([1, a])],
            ">": lambda a: ['>', self.rand.triangular(a+1, a*5)],
            ">=": lambda a: ['>=', self.rand.triangular(a, a*5)],
            "<": lambda a: ['<', self.rand.triangular(0, a-1)],
            "<=": lambda a: ['<=', self.rand.triangular(0, a)]}
        for symptom in _symptoms:
            res = self.dataSymptoms.find_one({'name': symptom}, {'_id': 1, 'name': 1, 'value': 1})
            if isinstance(res['value'], bool) or (not isinstance(res['value'], list) and (int(res['value']) > 10000000 or int(res['value']) < 10000000)):
                self.symptoms.append({"_id": res['_id'], "name": symptom, "value": self.rand.choice([True, False])})
            else:
                self.symptoms.append({"_id": res["_id"], "name": symptom, "value": self.gen_value[res['value'][0]](res['value'][1])})
