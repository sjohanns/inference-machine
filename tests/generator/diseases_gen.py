
import time
import random
import string

class GenDiseases():
    def __init__(self, d_min, d_max, s_min, s_max, db, alphabet, buffer):
        self.d_min = d_min
        self.d_max = d_max
        self.s_min = s_min
        self.s_max = s_max
        self.db = db
        self.symptoms = []
        self.diseases = []
        self.alphabet = alphabet
        self.buffer = buffer
        self.gen_symptoms_diseases()
    def name_symptoms(self):
        return 'S' + ''.join([self.alphabet[random.randint(0, 51)] for i in range(self.buffer)])
    def name_diseases(self):       return 'D' + ''.join([self.alphabet[random.randint(0, 51)] for i in range(self.buffer)])
    def gen_value(self):
        if random.choice([True, False]):
            return random.choice([True, False])
        else:
            return [random.choice(['>', '<', '<=', '>=', '=']), random.randint(-100, 100)]
    def gen_symptoms_diseases(self, old=False):
        '''
        base_disease = {
        'id': 0,
        'name': '',
        'symptoms': []
        }
        base_symptoms = {
        'id': 0,
        'name': '',
        'value': []
        }
        '''
        self.diseases = []
        self.symptoms = []
        memo = set()
        for i in range(random.randint(self.d_min, self.d_max)):
            dname = self.name_diseases()
            _id = hash(dname)
            dsymptoms = []
            if old:
                if symptoms:
                    for i in range(random.randint(1, 4)):
                        dsymptoms.append(random.choice([obj['id'] for obj in symptoms]))
            try:
                for j in range(random.randint(self.d_min, self.d_max)):
                    sname = self.name_symptoms()
                    __id = hash(sname)
                    if memo & {__id}:
                        raise ValueError('Dublicate')
                        break
                    else:
                        memo.add(__id)
                    value = self.gen_value()
                    dsymptoms.append(__id)
                    self.symptoms.append({'_id': __id, 'name': sname, 'value': value, 'cost': random.randint(0, 10) / 10})
            except ValueError as e:
                print(e)
            try:
                if memo & {_id}:
                    raise ValueError('Dublicate')
                else:
                    memo.add(_id)
                    self.diseases.append({'_id': _id, 'name': dname, 'symptoms': dsymptoms})
            except ValueError as e:
                print(e)
        time.sleep(2)
    def update(self):
        self.db['symptoms'].drop()
        self.db['diseases'].drop()
        self.db['symptoms'].insert_many(self.symptoms)
        self.db['diseases'].insert_many(self.diseases)
